import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profileform: FormGroup;
  constructor(private authService:AuthService, private router:Router) {

   }

  ngOnInit() {
    this.profileform = new FormGroup({
      username: new FormControl(''),
      password: new FormControl('')
    });
  }
onSubmit(formVal){
  this.authService.Login(formVal).then((res)=>{
    this.router.navigate(['Account']);
    console.log(res);
  }).catch((err)=>alert("username or passwaord wrong"));
}

}
